import 'dart:convert';
import 'dart:ui';
import 'package:http/http.dart' as http;

import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
        fontFamily: 'Permanent_Marker'
      ),
      home: DefaultTabController(
        length: 2,
        child: new Scaffold(
          body: TabBarView(
            children:[
              new Container(
                child:Stack(
                  fit: StackFit.expand,
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage("assets/images/bg-home.png"),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    Container(
                      child: new BackdropFilter(
                        filter: new ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
                        child: new Container(
                          decoration: new BoxDecoration(color: Colors.white.withOpacity(0.0)),
                        ),
                      ),
                    ),
                     FutureBuilder<Player>(
                          future: getPlayer(), //sets the getQuote method as the expected Future
                          builder: (context, snapshot) {
                            if (snapshot.hasData) { //checks if the response returns valid data              
                              return Center(
                                child: Column(
                                   mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      "Hi, ${snapshot.data.name}",
                                       style: new TextStyle(
                                        fontSize: 30,
                                        color: Colors.black
                                      ),
                                    ), //displays the quote
                                    Text("Trophies - ${snapshot.data.trophies}"), //displays the quote's author
                                    Text("Town Hall - ${snapshot.data.townHall}"), //displays the quote's author
                                  ],
                                ),
                              );
                            } else if (snapshot.hasError) { //checks if the response throws an error
                              return Text("${snapshot.error}");
                            }
                            return LinearProgressIndicator();
                          },
                        ),
                  ],
                )
              ),
             new Container(
                child:Stack(
                  fit: StackFit.expand,
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage("assets/images/bg-clan.jpeg"),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    Container(
                      child: new BackdropFilter(
                        filter: new ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
                        child: new Container(
                          decoration: new BoxDecoration(color: Colors.white.withOpacity(0.0)),
                        ),
                      ),
                    ),
                    Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                         FutureBuilder<Clan>(
                          future: getClan(), //sets the getQuote method as the expected Future
                          builder: (context, snapshot) {
                            if (snapshot.hasData) { //checks if the response returns valid data              
                              return Column(
                                  children: <Widget>[
                                    Text(
                                      "Hi, ${snapshot.data.name}",
                                       style: new TextStyle(
                                        fontSize: 30,
                                        color: Colors.black
                                      ),
                                    ), //displays the quote
                                    Text("Clan Level - ${snapshot.data.clanLevel}"), //displays the quote's author
                                  ],
                              );
                            } else if (snapshot.hasError) { //checks if the response throws an error
                              return Text("${snapshot.error}");
                            }
                            return LinearProgressIndicator();
                          },
                        ),
                         FutureBuilder<ClanWar>(
                          future: getCurrentWar(), //sets the getQuote method as the expected Future
                          builder: (context, snapshot) {
                            if (snapshot.hasData) { //checks if the response returns valid data              
                              return Column(
                                  children: <Widget>[
                                    Text(
                                      "State: ${snapshot.data.state}",
                                    ), //displays the quote
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                      children:<Widget>[
                                        Text("Clan Attacks - ${snapshot.data.myAttacks}"), //displays the quote's author
                                        Text("Clan Stars - ${snapshot.data.myStars}"),
                                      ]
                                    ), //displays the quote's author
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                      children:<Widget>[
                                        Text("Opponent Attacks - ${snapshot.data.opponentAttacks}"), //displays the quote's author
                                        Text("Opponent Stars - ${snapshot.data.opponentStars}"), //displays the quote's author
                                      ]
                                    ), //
                                  ],
                              );
                            } else if (snapshot.hasError) { //checks if the response throws an error
                              return Text("${snapshot.error}");
                            }
                            return LinearProgressIndicator();
                          },
                        ),
                      ],
                      )
                    
                       
                  ],
                )
              ),
              
            ],
          ),
          bottomNavigationBar: new TabBar(
            tabs: [
              new Tab(
                icon: new Icon(
                  Icons.person
                )
              ),
              new Tab(
                icon: new Icon(
                  Icons.group
                )
              )
            ],
            labelColor: Colors.red,
            unselectedLabelColor: Colors.red[200],
            indicatorSize: TabBarIndicatorSize.label,
            indicatorPadding: EdgeInsets.all(5.0),
            indicatorColor: Colors.red,
          ),
          backgroundColor:Colors.yellow,
        )
      ),
   
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          // Column is also layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.display1,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

 Future<Player> getPlayer() async {
    String url = 'https://api.clashofclans.com/v1/players/%20LVY22UR2';
    final response =
        await http.get(url, headers: {"Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiIsImtpZCI6IjI4YTMxOGY3LTAwMDAtYTFlYi03ZmExLTJjNzQzM2M2Y2NhNSJ9.eyJpc3MiOiJzdXBlcmNlbGwiLCJhdWQiOiJzdXBlcmNlbGw6Z2FtZWFwaSIsImp0aSI6IjQzOTJiMzdlLTk4MjgtNGFhMC04ZGY3LTFiNjQ0ZDgwODUyYiIsImlhdCI6MTU3MjYzMTI1NSwic3ViIjoiZGV2ZWxvcGVyLzZlMTJmM2MyLTExMjgtNDBmYi03ZWVlLTMwMmU0ZTU0NTAyZiIsInNjb3BlcyI6WyJjbGFzaCJdLCJsaW1pdHMiOlt7InRpZXIiOiJkZXZlbG9wZXIvc2lsdmVyIiwidHlwZSI6InRocm90dGxpbmcifSx7ImNpZHJzIjpbIjgwLjEwOC43LjI0OSJdLCJ0eXBlIjoiY2xpZW50In1dfQ.UNlvtaYaqj5fsHb2LEnPdTENjhVFJUOPBCdWiHaNFnaOkyftq3ieQLz8vJ3cwYLSJzPOHWvXEQZavHHWgftI9A"});


    if (response.statusCode == 200) {
      return Player.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to load player');
    }
  }

 Future<Clan> getClan() async {
    String url = 'https://api.clashofclans.com/v1/clans/%2020r9uc89y';
    final response =
        await http.get(url, headers: {"Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiIsImtpZCI6IjI4YTMxOGY3LTAwMDAtYTFlYi03ZmExLTJjNzQzM2M2Y2NhNSJ9.eyJpc3MiOiJzdXBlcmNlbGwiLCJhdWQiOiJzdXBlcmNlbGw6Z2FtZWFwaSIsImp0aSI6IjQzOTJiMzdlLTk4MjgtNGFhMC04ZGY3LTFiNjQ0ZDgwODUyYiIsImlhdCI6MTU3MjYzMTI1NSwic3ViIjoiZGV2ZWxvcGVyLzZlMTJmM2MyLTExMjgtNDBmYi03ZWVlLTMwMmU0ZTU0NTAyZiIsInNjb3BlcyI6WyJjbGFzaCJdLCJsaW1pdHMiOlt7InRpZXIiOiJkZXZlbG9wZXIvc2lsdmVyIiwidHlwZSI6InRocm90dGxpbmcifSx7ImNpZHJzIjpbIjgwLjEwOC43LjI0OSJdLCJ0eXBlIjoiY2xpZW50In1dfQ.UNlvtaYaqj5fsHb2LEnPdTENjhVFJUOPBCdWiHaNFnaOkyftq3ieQLz8vJ3cwYLSJzPOHWvXEQZavHHWgftI9A"});


    if (response.statusCode == 200) {
      return Clan.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to load clan');
    }
  }

 Future<ClanWar> getCurrentWar() async {
    String url = 'https://api.clashofclans.com/v1/clans/%2320r9uc89y/currentwar';
    final response =
        await http.get(url, headers: {"Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiIsImtpZCI6IjI4YTMxOGY3LTAwMDAtYTFlYi03ZmExLTJjNzQzM2M2Y2NhNSJ9.eyJpc3MiOiJzdXBlcmNlbGwiLCJhdWQiOiJzdXBlcmNlbGw6Z2FtZWFwaSIsImp0aSI6IjQzOTJiMzdlLTk4MjgtNGFhMC04ZGY3LTFiNjQ0ZDgwODUyYiIsImlhdCI6MTU3MjYzMTI1NSwic3ViIjoiZGV2ZWxvcGVyLzZlMTJmM2MyLTExMjgtNDBmYi03ZWVlLTMwMmU0ZTU0NTAyZiIsInNjb3BlcyI6WyJjbGFzaCJdLCJsaW1pdHMiOlt7InRpZXIiOiJkZXZlbG9wZXIvc2lsdmVyIiwidHlwZSI6InRocm90dGxpbmcifSx7ImNpZHJzIjpbIjgwLjEwOC43LjI0OSJdLCJ0eXBlIjoiY2xpZW50In1dfQ.UNlvtaYaqj5fsHb2LEnPdTENjhVFJUOPBCdWiHaNFnaOkyftq3ieQLz8vJ3cwYLSJzPOHWvXEQZavHHWgftI9A"});


    if (response.statusCode == 200) {
      return ClanWar.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to load clan');
    }
  }

class Player {
  final String name;
  final int trophies;
  final int townHall;

  Player({this.name, this.trophies, this.townHall});

  factory Player.fromJson(Map<String, dynamic> json) {
    return Player(
        name: json['name'],
        trophies: json['trophies'],
        townHall: json['townHallLevel']
    );
  }
}

class Clan {
  final String name;
  final int clanLevel;

  Clan({this.name, this.clanLevel});

  factory Clan.fromJson(Map<String, dynamic> json) {
    return Clan(
        name: json['name'],
        clanLevel: json['clanLevel']
    );
  }
}

class ClanWar {
  final String state;
  final int myAttacks;
  final int myStars;
  final int opponentAttacks;
  final int opponentStars;

  ClanWar({this.state, this.myAttacks, this.myStars, this.opponentAttacks, this.opponentStars});

  factory ClanWar.fromJson(Map<String, dynamic> json) {
    return ClanWar(
        state: json['state'],
        myAttacks: json['clan']['attacks'],
        myStars: json['clan']['stars'],
        opponentAttacks: json['opponent']['attacks'],
        opponentStars: json['opponent']['stars'],
    );
  }
}
